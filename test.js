var assert = require('assert');
var reverseBinary = require('./reverse_binary')

it('13 in binary should be 1101 and reversing it gives 1011 which corresponds to number 11', function() {
  assert.equal(reverseBinary.reverseBinary(13), 11);
});