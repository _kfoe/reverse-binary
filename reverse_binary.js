module.exports = {
  reverseBinary: (number) => {
    // Calculate binary
    let toBinary = number.toString(2);

    // Reverse
    toBinary = Array.from(String(toBinary), (n) => n).reverse().join("");

    // Convert to decimal
    let toDecimal = parseInt(toBinary, 2);

    //Return
    return parseInt(toDecimal, 10);
  }
}